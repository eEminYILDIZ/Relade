﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AEOSIFG.Models;
using Device.Code;
using Microsoft.AspNetCore.Mvc;

namespace Device.Controllers
{
    [Route("api/[controller]")]
    public class ServiceController : Controller
    {
        public ServiceController()
        {
            Console.WriteLine("constructer test");
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public string Post([FromBody]MessageToDeviceModel value)
        {
            ServiceOperations operations = new ServiceOperations();
            string _return = operations.TakeFromWebApi(value);

            return _return;
        }

       
    }
}
