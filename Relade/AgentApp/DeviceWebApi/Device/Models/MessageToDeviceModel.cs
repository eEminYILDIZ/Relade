﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEOSIFG.Models
{
    public class MessageToDeviceModel
    {
        public string productCode { get; set; }
        public string property { get; set; }
        public string value { get; set; }
    }
}
