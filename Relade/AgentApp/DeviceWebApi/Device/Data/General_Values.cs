﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Device.Data
{
    public class General_Values
    {
        public static string controlSystemIP { get { return "localhost"; } }
        public static string deviceWebApiListenPort { get { return "1720"; } }
        public static string productCode { get { return "smsng-tv-e7400"; }  }

        public static int deviceListenPort { get { return 10001; } }
        public static int deviceSendPort { get { return 10002; } }
    }
}
