﻿using AEOSIFG.Models;
using Device.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Device.Code
{
    public class DeviceOperations
    {
        public void TakeFromDevice()
        {
            // infinity loop
            while (true)
            {
                TcpListener serverSocket = new TcpListener(General_Values.deviceListenPort);
                


                while (true)
                {
                    TcpClient clientSocket = default(TcpClient);
                    serverSocket.Start();

                    clientSocket = serverSocket.AcceptTcpClient();
                    NetworkStream networkStream = clientSocket.GetStream();

                    byte[] receivedData = new byte[65535];
                    networkStream.Read(receivedData, 0, (int)clientSocket.ReceiveBufferSize-1);


                    // getting object from json string data
                    string stringData = Encoding.UTF8.GetString(receivedData);
                    MessageToDeviceModel message = JsonConvert.DeserializeObject<MessageToDeviceModel>(stringData);

                    // send obtained message to AutomationControlSystem via webApi
                    ServiceOperations servop = new ServiceOperations();
                    servop.SendToControlSystemWebApi_Message(message);
                    
                    clientSocket.Close();
                }

                serverSocket.Stop();

            }
        }

        public string SendToDevice(MessageToDeviceModel message)
        {
            // getting string of message object as json
            string serialized_message = JsonConvert.SerializeObject(message);
            
            // opening socket for connection
            Socket baglan = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            baglan.Connect(IPAddress.Parse("127.0.0.1"), General_Values.deviceSendPort);

            // getting byte of serialized message string
            byte[] dizi = Encoding.UTF8.GetBytes(serialized_message);

            // sending data to terminal receiver
            baglan.Send(dizi, 0, dizi.Length, SocketFlags.None);

            // get response data from device 
            byte[] receivedData = new byte[1024];
            baglan.Receive(receivedData);

            // close connection
            baglan.Close();

            
            // return received string
            //string reponseDataString = Encoding.UTF8.GetString(receivedData);
            //return reponseDataString;

            return "success";
        }
    }
}
