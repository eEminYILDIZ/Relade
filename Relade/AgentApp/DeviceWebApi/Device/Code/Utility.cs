﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Device.Code
{
    public class Utility
    {
        public static string GetHostIP()
        {
            string _return = "";
            string strHostName = Dns.GetHostName();
            IPHostEntry ipEntry = Dns.GetHostByName(strHostName);
            IPAddress[] IpAddr = ipEntry.AddressList;
            for (int i = 0; i < IpAddr.Length; i++)
            {
                string ip = IpAddr[i].ToString();
                if (ip.IndexOf(":") == -1)
                {
                    _return = IpAddr[i].ToString();
                }

            }
            return _return;
        }
    }
}
