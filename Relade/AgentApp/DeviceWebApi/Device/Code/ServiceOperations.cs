﻿using AEOSIFG.Models;
using Device.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Device.Code
{
    public class ServiceOperations
    {
        public ServiceOperations()
        {
        }

        public string TakeFromWebApi(MessageToDeviceModel value)
        {
            DeviceOperations deviceOps = new DeviceOperations();
            string result = deviceOps.SendToDevice(value);

            return result;
        }

        public string SendToControlSystemWebApi_Message(MessageToDeviceModel message)
        {
            try
            {
                message.productCode = General_Values.productCode;

                string messageJsonString = JsonConvert.SerializeObject(message);
                string deviceIP = General_Values.controlSystemIP;
                string port = General_Values.deviceWebApiListenPort;
                // HTTP Post Request to Device Service
                var request = (HttpWebRequest)WebRequest.Create("http://" + deviceIP + ":" + port + "/api/Message");

                var postData = messageJsonString;
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (responseString == "success")
                    return "success";
                else
                    return "failed";

            }
            catch (Exception ex)
            {
                return "server_error";
            }

        }


        public string SendToWebApi_IP(DeviceIp value)
        {
            try
            {
                string messageJsonString = JsonConvert.SerializeObject(value);
                string controlSystemWebApiIp = General_Values.controlSystemIP;
                string controlSystemWebApiPort = General_Values.deviceWebApiListenPort;
                // HTTP Post Request to Device Service
                var request = (HttpWebRequest)WebRequest.Create("http://" + controlSystemWebApiIp + ":" + controlSystemWebApiPort + "/api/IP");

                var postData = messageJsonString;
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (responseString == "success")
                    return "success";
                else
                    return "failed";

            }
            catch (Exception ex)
            {
                return "server_error";
            }

        }
    }
}
