﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AEOSIFG.Models;
using Device.Code;
using Device.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Device
{
    public class Program
    {
        public static void Main(string[] args)
        {

            // run this actions with thread
            Task.Run(() =>
            {

                try
                {
                    // get host system ip than send it to Control System via C.S.WebApi
                    // string ip = Utility.GetHostIP();
                    string ip = "localhost";

                    DeviceIp devIp = new DeviceIp()
                    {
                        deviceCode = General_Values.productCode,
                        IP = ip
                    };
                    ServiceOperations sorp = new ServiceOperations();
                    sorp.SendToWebApi_IP(devIp);

                    // enter the infinity listen loop(server client communication)
                    DeviceOperations deviceOperations = new DeviceOperations();
                    deviceOperations.TakeFromDevice();
                }
                catch (Exception ex)
                {
                }
            });


            // run the webApi code
            BuildWebHost(args).Run();

        }
        

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
