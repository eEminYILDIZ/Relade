/* 
  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
*/

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

int led_animate=       13;
int button_isOpen=     A5;
int button_channelUp=  A1;
int button_channelDown=A2;
int button_volumeUp=   A3;
int button_volumeDown= A4;


int channel=0,volume=0;
char isOpen[]="ACIK    ";
String data;

//--------------------------------------------------------------



void WriteValuesToLCD(){
  if(strcmp(isOpen,"ACIK")==0)
  {
    lcd.setCursor(0, 0);
    lcd.print("Channel:        ");
    lcd.setCursor(9, 0);
    lcd.print(channel);
    
    lcd.setCursor(0, 1);
    lcd.print("Volume :        ");
    lcd.setCursor(9, 1);
    lcd.print(volume);
  }
  else
  {
    lcd.clear();
  }
    
}

void AnimateLed(){
    digitalWrite(led_animate, HIGH);
    delay(300);
    digitalWrite(led_animate, LOW);
}

int OperateDescent(int number)
{
  if(number==0)
    return 0;
  else
    return --number;
}
boolean Operate_OpenClose(){
  if(strcmp(isOpen,"ACIK")==0)
    strcpy(isOpen, "KAPALI");
  else
    strcpy(isOpen, "ACIK");
}

void SendChannel(){
  Serial.print("channel:");
  Serial.print(channel);  
  Serial.print("\n"); 
}
void SendVolume(){
  Serial.print("volume:");
  Serial.print(volume);  
  Serial.print("\n"); 
}
void SendIsOpen(){
  Serial.print("isOpen:");
  Serial.print(isOpen);  
  Serial.print("\n"); 
}


//______________________________________________________________

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    Serial.write("channel\n");
    
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

//_____________________________________________________________

//-------------------------------------------------------------

void setup() {
  Serial.begin(9600);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("Samsung Smart TV");
  delay(3000);
  strcpy(isOpen,"ACIK");
  WriteValuesToLCD();
}
int flag=0;
int flag2=0;
void loop() {

  if(digitalRead(button_isOpen) == HIGH)
  {
//    AnimateLed();
//    Operate_OpenClose();
//    SendIsOpen();  
//    WriteValuesToLCD();
  }
  if(digitalRead(button_channelUp) == HIGH)
  {
    AnimateLed();
    channel++;
    SendChannel();
    WriteValuesToLCD();
  }
  if(digitalRead(button_channelDown) == HIGH)
  {
    AnimateLed();
    channel = OperateDescent(channel);
    SendChannel();
    WriteValuesToLCD();
  }
  if(digitalRead(button_volumeUp) == HIGH)
  {
    AnimateLed();
    volume++;
    SendVolume();
    WriteValuesToLCD();
  }
  if(digitalRead(button_volumeDown) == HIGH)
  {
    AnimateLed();
    volume = OperateDescent(volume);
    SendVolume();
    WriteValuesToLCD();
  }
  int value=0;
  if(Serial.available() > 0)
  {
      if(flag2==0)
      {
          data = Serial.readString();  
          
            
        if(data=="channel")
        {
         flag=1;
         flag2=1;
        } 
        
        if(data=="volume")
        {
         flag=2;
        flag2=1;
        } 
        
         if(data=="isOpen")
        {
         flag=3;
         flag2=1;
        }
    } 
    else
    {
      value = Serial.read();
      
      switch(flag){
        case 1:
          channel=value-48;
          break;
         case 2:
          volume=value-48;
          break;
          
       case 3:
          if(value=='A')
              strcpy(isOpen,"ACIK");
          if(value=='K')
              strcpy(isOpen,"KAPALI");
          break;
      }
      
      flag2=0;
      WriteValuesToLCD();
    }
  }
}
