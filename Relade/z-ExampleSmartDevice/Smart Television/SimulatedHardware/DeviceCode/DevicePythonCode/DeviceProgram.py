# Thread icin kutuphanaler:
import threading

# komut alimi icin kutuphaneler:
from time import sleep

# veri gonderimi icin kutuphaneler:
import serial

import json

# _____________________________________________________________

from socket import *

# will use for listening data from WebApi
s = socket(AF_INET, SOCK_STREAM)
s.bind(("localhost", 5002))
s.listen(1)

# will use for sending data to WebApi
sock = socket(AF_INET, SOCK_STREAM)
server_address = ('localhost', 5001)

# _____________________________________________________________



#veri gonderimi icin tanimlamalar:
kontrol_karti = serial.Serial('/dev/ttyUSB0', 9600)
kontrol_karti.close()
kontrol_karti.open()

productCode="smsng-tv-e7400"
is_1_devam = 1
is_2_devam = 1

# scenerio:
# 1: read data from WebApi
#  : close connection
# 2: deserialize data to object
# 3: make decide to action
# 4: do action on arduino
def function():
    while is_2_devam == 1:
        # accepting connection and reading data from coming connection:
        c, a = s.accept()
        text = c.recv(1000)
        # close coming connection


        # build object from json data:
        WebApiMessage = json.loads(text)

        # switch property to action:
        messageProperty = WebApiMessage["property"];
        messageValue = WebApiMessage["value"];

        # this comment will open when arduino connected physically
        # send data to arduino
        writeData = messageProperty[:1] + messageValue
        kontrol_karti.write(str(writeData))
        
        # testCode:
        outputText="Device " + messageProperty + " status changed to: " + messageValue
        print(outputText)
        
        # send response to did action
        # this data changes values on control system
        dataObject = {"productCode": productCode, "property": messageProperty, "value": messageValue}
        dataString = json.dumps(dataObject)
        WebApiVeriGonder(dataString)

        c.sendall(outputText)
        c.close()

    return


def veri_gonder():

    count_i = 5

    while is_1_devam == 1:
        # this comment will open when arduino connected physical
        # reading data from arduino:
        arduinoData=kontrol_karti.readline()

        # testCode:
        # arduinoData = "channel:" + str(count_i);
        # count_i = count_i + 1

        propertyData, valueData = arduinoData.split(":")

        # building a message Object and getting json string data of message object
        dataObject = {"productCode": productCode, "property": propertyData, "value": valueData}
        dataString = json.dumps(dataObject)

        # dataString="{'productCode':'smsng-tv-e7400','operationCode':'','information':[{'key':'"+propertyData+"','value':'"+valueData+"'}]}"
        print("Send Data: "+dataString)

        WebApiVeriGonder(dataString)
        
        # send an notification to user
        count_i++;
        if count_i%10==0:
        	dataObject ={  Id:str(count_i*2), type:"info", datetime:"11.06.2018 15:17", deviceCode:productCode, content:"Daha fazla kanal için frekansları ile beraber kanal eklemesi yapabilirsiniz."} 
        	dataString = json.dumps(dataObject)
			WebApiVeriGonder(dataString)

    return


# ________________________________________________________


def WebApiVeriGonder(data):
	# open WebApi connection
    sock = socket(AF_INET, SOCK_STREAM)
    sock.connect(server_address)
    # send data to WebApi
    sock.sendall(data)
    sock.close()
    return



# MAIN Program


# threads:
is_1 = threading.Thread(target=veri_gonder)
is_2 = threading.Thread(target=function)
is_1.start()
print("thread 1 started.")
is_2.start()
print("thread 2 started.")

# for closing program:
a = raw_input("Cikmak icin 'x' e basin... \n")
if a == "x":
    is_1_devam = 0
    is_2_devam = 0

# close arduino connection
kontrol_karti.close();