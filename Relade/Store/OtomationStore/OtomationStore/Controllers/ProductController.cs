﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using OtomationStore.Code;
using OtomationStore.Models;

namespace OtomationStore.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Details(string id)
        {
            storedbEntities db = new storedbEntities();

            products product = db.products.FirstOrDefault(w => w.product_code == id);

            ViewData["product"] = product;

            return View();
        }

        public ActionResult ListAllProducts()
        {
            storedbEntities db = new storedbEntities();

            List<products> products = db.products.OrderBy(w => w.producers.producers_name).ToList();

            ViewData["products"] = products;

            return View();
        }

        public ActionResult AddProduct()
        {
            string producer_name = Session["producer_name"].ToString();
            ViewData["producer_name"] = producer_name;
            return View();
        }

        [HttpPost]
        public ActionResult Transition_AddProduct(FormCollection formCollection)
        {
            string producerName = Session["producer_name"].ToString();
            try
            {
                products product = (products)MTranslation.BuildObject(formCollection, "products");
                product.download_count = 0;
                product.publish_date = DateTime.Now;

                // creating directory
                string product_directory= Path.Combine(Server.MapPath("~/Storage"), producerName, product.product_code);
                Directory.CreateDirectory(product_directory);

                // adding picture to Disk
                bool flagImage = false, flagPackage = false, flagJsonFile = false;
                for (int i = 0; i < 2;i++ )
                {
                    HttpPostedFileBase file = Request.Files[i];

                    if (file.ContentLength > 0)
                    {
                        string filename = Path.GetFileName(file.FileName);
                        if (filename.EndsWith(".zip"))
                        {
                            string extention = Path.GetExtension(filename);
                            string path = Path.Combine(Server.MapPath("~/Storage"), producerName, product.product_code,
                                product.product_code + extention);
                            file.SaveAs(path);

                            product.package_file_path = product.product_code + extention;

                            flagPackage = true;
                        }
                        else if (filename.EndsWith(".jpg") || filename.EndsWith(".png") || filename.EndsWith(".bmp"))
                        {
                            string extention = Path.GetExtension(filename);
                            string path = Path.Combine(Server.MapPath("~/Storage"), producerName, product.product_code,
                                product.product_code + extention);
                            file.SaveAs(path);

                            product.image_path = product.product_code + extention;

                            flagImage = true;
                        }
                    }
                }

                // creating json file
                Serialiable_Product _product = new Serialiable_Product()
                {
                    id = product.id,
                    product_code = product.product_code,
                    producer_name = product.producer_name,
                    image_path = product.image_path,
                    title = product.title,
                    publish_date = product.publish_date,
                    package_file_path = product.package_file_path,
                    explain_text = product.explain_text,
                    download_count = product.download_count,
                    permissions = product.permissions,
                    version = product.version
                };
                string jsonProduct = JsonConvert.SerializeObject(_product);
                string jsonFilePath = Path.Combine(Server.MapPath("~/Storage"), producerName, product.product_code, product.product_code + ".json");
                using (StreamWriter write = new StreamWriter(jsonFilePath))
                {
                    write.Write(jsonProduct);
                    write.Close();

                    flagJsonFile = true;
                }

                

                if (flagImage && flagPackage && flagJsonFile)
                {
                    // add product to db
                    storedbEntities db = new storedbEntities();
                    db.products.Add(product);
                    db.SaveChanges();

                    return RedirectToAction("Details", new { id = product.product_code });
                }
                else
                {
                    TempData["message"] = new MessageModel("Adding new Product Failed", "File Not Found For Uploading ", Message_Type.Error);
                    return RedirectToAction("AddProduct");
                }
            }
            catch (Exception exception)
            {
                TempData["message"] = new MessageModel("Register Failed", "An Error Occured: " + exception.Message, Message_Type.Error);
                return RedirectToAction("Register");
            }
        }
    }
}