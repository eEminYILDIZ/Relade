﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.AccessControl;
using System.Web.Http;
using OtomationStore.Models;

namespace OtomationStore.Controllers
{
    public class ServiceController : ApiController
    {
        public Serialiable_Product Get(string id)
        {
            storedbEntities db = new storedbEntities();
            products product=  db.products.FirstOrDefault(w => w.product_code == id);
            Serialiable_Product _product = new Serialiable_Product()
            {
                id = product.id,
                product_code = product.product_code,
                producer_name = product.producer_name,
                image_path = product.image_path,
                title = product.title,
                publish_date = product.publish_date,
                package_file_path = product.package_file_path,
                explain_text = product.explain_text,
                download_count = product.download_count,
                permissions = product.permissions,
                version = product.version
            };

            return _product;
        }
    }
}