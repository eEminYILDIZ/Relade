﻿using System;
using System.Linq;
using System.Web.Mvc;
using OtomationStore.Code;
using OtomationStore.Models;
using System.Collections.Generic;
using System.IO;

namespace OtomationStore.Controllers
{
    public class ProducerController : Controller
    {
        // GET: Producer
        public ActionResult Index(string id)
        {
            if (Session["name"] == null)
                return RedirectToAction("Login");

            string pname = Session["producer_name"].ToString();

            storedbEntities db=new storedbEntities();
            producers producer = db.producers.FirstOrDefault(w => w.producers_name == pname);
            ViewData["producer"]=producer;

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Transition_Register(FormCollection formCollection)
        {
            try
            {
                producers producer = (producers)MTranslation.BuildObject(formCollection, "producers");
                producer.registration_date=DateTime.Now;

                storedbEntities db = new storedbEntities();
                db.producers.Add(producer);
                db.SaveChanges();

                return RedirectToAction("Login");
            }
            catch (Exception exception)
            {
                TempData["message"] = new MessageModel("Register Failed", "An Error Occured: " + exception.Message, Message_Type.Error);
                return RedirectToAction("Register");
            }
        }

        [HttpPost]
        public ActionResult Transition_Login(FormCollection formCollection)
        {
            try
            {
                producers producer = (producers)MTranslation.BuildObject(formCollection, "producers");

                storedbEntities db = new storedbEntities();

                producers mapped_producers = db.producers.FirstOrDefault(w =>
                    w.producers_name == producer.producers_name && w.password == producer.password);

                if (mapped_producers != null) // yani bu bilgilere sahip bir kullanıcı var ise:
                {
                    Session.Add("producer_name", mapped_producers.producers_name);
                    Session.Add("name", mapped_producers.name);

                    return RedirectToAction("ListProducts");
                }
                else
                {
                    // for inpage error
                    TempData["_message"] = "Producer Name or Password is Incorrect";
                    return RedirectToAction("Login");
                }
            }
            catch (Exception exception)
            {
                // for loyout error
                TempData["message"] = new MessageModel("Register Failed", "An Error Occured: " + exception.Message, Message_Type.Error);
                return RedirectToAction("Login");
            }
        }

        public ActionResult ListProducts()
        {
            string producer_name = Session["producer_name"].ToString();

            storedbEntities db = new storedbEntities();
            List<products> products = db.products.Where(w => w.producer_name == producer_name).ToList();

            ViewData["products"] = products;

            return View();
        }

        public ActionResult Transition_DeleteProduct(string id)
        {
            try
            {
                storedbEntities db = new storedbEntities();
                products product = db.products.FirstOrDefault(w => w.product_code == id);

                string product_name = id;
                if (product != null)
                {
                    product_name = product.title;

                    // Delete product Files from FileSystem(Storage)
                    string product_path = Path.Combine(Server.MapPath("/Storage"), product.producers.producers_name,
                        product.product_code);
                    if (System.IO.Directory.Exists(product_path))
                        Directory.Delete(product_path, true); // delete product directory with their entries

                    // delete product from DB
                    db.products.Remove(product);
                    db.SaveChanges();

                    TempData["message"] = new MessageModel("Info", product_name + " DELETED.", Message_Type.Success);
                }
                else
                {
                    TempData["message"] = new MessageModel("Info", product_name + ": PRODUCT NOT FOUND", Message_Type.Success);
                }
            }
            catch (Exception exception)
            {
                TempData["message"] = new MessageModel("Info", "An Error Accured: " + exception.Message, Message_Type.Success);
            }

            return RedirectToAction("ListProducts");
        }
    }
}