//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OtomationStore.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class products
    {
        public int id { get; set; }
        public string product_code { get; set; }
        public string producer_name { get; set; }
        public string version { get; set; }
        public System.DateTime publish_date { get; set; }
        public string title { get; set; }
        public string explain_text { get; set; }
        public string permissions { get; set; }
        public int download_count { get; set; }
        public string package_file_path { get; set; }
        public string image_path { get; set; }
    
        public virtual producers producers { get; set; }
    }
}
