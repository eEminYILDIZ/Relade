﻿namespace OtomationStore.Models
{

    public enum Message_Type
    {
        Success,
        Error,
        Information,
        Warning
    }

    public class MessageModel
    {
        public Message_Type type { get; set; }
        public string caption { get; set; }
        public string message { get; set; }

        public MessageModel(string p_caption,string p_message,Message_Type p_type=Message_Type.Information)
        {
            caption = p_caption;
            type = p_type;
            message = p_message;
        }


        public string MessageTypeToString()
        {
            string message_type_css_string = "";

            switch (type)
            {
                case Message_Type.Error:
                    message_type_css_string = " alert-danger";
                    break;

                case Message_Type.Success:
                    message_type_css_string = " alert-success";
                    break;

                case Message_Type.Information:
                    message_type_css_string = " alert-info";
                    break;

                case Message_Type.Warning:
                    message_type_css_string = " alert-warning";
                    break;

                default:
                    message_type_css_string = " alert-info";
                    break;
            }

            return message_type_css_string;
        }
    }
}