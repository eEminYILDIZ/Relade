﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OtomationStore.Models
{
    public class Serialiazable_Classes
    {
    }

    public class Serialiable_Product
    {
        public int id { get; set; }
        public string product_code { get; set; }
        public string producer_name { get; set; }
        public string version { get; set; }
        public System.DateTime publish_date { get; set; }
        public string title { get; set; }
        public string explain_text { get; set; }
        public string permissions { get; set; }
        public int download_count { get; set; }
        public string package_file_path { get; set; }
        public string image_path { get; set; }
    }
}