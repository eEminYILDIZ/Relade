USE [master]
GO
/****** Object:  Database [otomationstoredb]    Script Date: 22.02.2018 12:30:46 AM ******/
CREATE DATABASE [otomationstoredb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'otomationstoredb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\otomationstoredb.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'otomationstoredb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\otomationstoredb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [otomationstoredb] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [otomationstoredb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [otomationstoredb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [otomationstoredb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [otomationstoredb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [otomationstoredb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [otomationstoredb] SET ARITHABORT OFF 
GO
ALTER DATABASE [otomationstoredb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [otomationstoredb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [otomationstoredb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [otomationstoredb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [otomationstoredb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [otomationstoredb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [otomationstoredb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [otomationstoredb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [otomationstoredb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [otomationstoredb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [otomationstoredb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [otomationstoredb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [otomationstoredb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [otomationstoredb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [otomationstoredb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [otomationstoredb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [otomationstoredb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [otomationstoredb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [otomationstoredb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [otomationstoredb] SET  MULTI_USER 
GO
ALTER DATABASE [otomationstoredb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [otomationstoredb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [otomationstoredb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [otomationstoredb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [otomationstoredb]
GO
/****** Object:  Table [dbo].[producers]    Script Date: 22.02.2018 12:30:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[producers](
	[producers_name] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[country] [nvarchar](50) NOT NULL,
	[branch] [nvarchar](50) NOT NULL,
	[registration_date] [datetime] NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[phone] [nvarchar](50) NOT NULL,
	[website] [nvarchar](50) NOT NULL,
	[address] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_producers_1] PRIMARY KEY CLUSTERED 
(
	[producers_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[products]    Script Date: 22.02.2018 12:30:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[products](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[product_code] [nvarchar](50) NOT NULL,
	[producer_name] [nvarchar](50) NOT NULL,
	[version] [nvarchar](20) NOT NULL,
	[publish_date] [datetime] NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[explain_text] [nvarchar](1000) NOT NULL,
	[permissions] [nvarchar](300) NOT NULL,
	[download_count] [int] NOT NULL,
	[package_file_path] [nvarchar](200) NOT NULL,
	[image_path] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_products] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[producers] ([producers_name], [password], [name], [country], [branch], [registration_date], [email], [phone], [website], [address]) VALUES (N'arcelik', N'0123', N'Arçelik A.Ş.', N'Turkey', N'Consumer Elektronic', CAST(0x0000A85B00000000 AS DateTime), N'mail@arcelik.com.tr', N'+90216785187', N'www.arcelik.com.tr', N'İstanbul / Türkiye')
INSERT [dbo].[producers] ([producers_name], [password], [name], [country], [branch], [registration_date], [email], [phone], [website], [address]) VALUES (N'samsung', N'0123', N'Samsung Inc.', N'Korea', N'Consumer Electronic', CAST(0x0000A85B00000000 AS DateTime), N'info@samsung.com', N'+10548724678797', N'www.samsung.com', N'Seul / Korea')
SET IDENTITY_INSERT [dbo].[products] ON 

INSERT [dbo].[products] ([id], [product_code], [producer_name], [version], [publish_date], [title], [explain_text], [permissions], [download_count], [package_file_path], [image_path]) VALUES (1, N'smsng-tv-e7400', N'samsung', N'2.0.3', CAST(0x0000A85C00000000 AS DateTime), N'Samsung e7400 Full HD Smart TV', N'this is korean technology', N'electric', 1, N'smsng-tv-e7400_203.zip', N'smsng-tv-e7400.jpg')
INSERT [dbo].[products] ([id], [product_code], [producer_name], [version], [publish_date], [title], [explain_text], [permissions], [download_count], [package_file_path], [image_path]) VALUES (6, N'arclk-wm-k3000', N'arcelik', N'3.2.5', CAST(0x0000A85C00000000 AS DateTime), N'Arçelik 9KG A Sınıfı Çamaşır Makinesi', N'just technology', N'noise', 1, N'arclk-wm-k3000_325.zip', N'arclk-wm-k3000.jpg')
SET IDENTITY_INSERT [dbo].[products] OFF
ALTER TABLE [dbo].[products]  WITH CHECK ADD  CONSTRAINT [FK_products_producers] FOREIGN KEY([producer_name])
REFERENCES [dbo].[producers] ([producers_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[products] CHECK CONSTRAINT [FK_products_producers]
GO
USE [master]
GO
ALTER DATABASE [otomationstoredb] SET  READ_WRITE 
GO
