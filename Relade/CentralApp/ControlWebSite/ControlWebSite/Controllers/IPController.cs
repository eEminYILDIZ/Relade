﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWebSite.Code;
using ControlWebSite.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace ControlWebSite.Controllers
{
    [Route("api/IP")]
    public class IPController : Controller
    {
        private readonly IDistributedCache _distributedCache;

        public IPController(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(string id)
        {
            Coordinator coordinator = new Coordinator(_distributedCache);
            string nm = coordinator.ForSignalR_ChatHub_GetDeviceIP(id);

            return nm;

            //return "not implemented code";
        }


        // POST api/<controller>
        [HttpPost]
        public string Post([FromBody]DeviceIp value)
        {
            try
            {
                // notfity coordinator
                Coordinator coordinator = new Coordinator(_distributedCache);
                int result = coordinator.ForWebApi_SetDeviceIP(value);

                if (result == 1)
                    return "success";
                else
                    return "failed";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }

    }
}