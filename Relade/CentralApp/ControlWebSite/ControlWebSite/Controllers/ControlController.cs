﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ControlWebSite.Code;
using ControlWebSite.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace ControlWebSite.Controllers
{
    public class ControlController : Controller
    {
        private IHostingEnvironment _env;
        private readonly IDistributedCache _distributedCache;

        public ControlController(IHostingEnvironment env, IDistributedCache distributedCache)
        {
            _env = env;
            _distributedCache = distributedCache;
        }

        // bütün uygulamaları listeleyeceğimiz kısım
        public IActionResult Launch()
        {
            List<DeviceModel> deviceApss = GetDeviceApps();
            ViewData["deviceApps"] = deviceApss;

            return View();
        }

        public IActionResult Device(string id)
        {
            ViewData["id"] = id;
            return View();
        }

        public IActionResult Alarms()
        {
            Coordinator coordinator = new Coordinator(_distributedCache);
            NotificationModel nm = coordinator.ForNotificationHub_GetAllNotifications();
            ViewData["notificationModel"]=nm;

            return View();
        }
        
        public IActionResult Settings()
        {
            return View();
        }


        public IActionResult NewDevices()
        {
            Coordinator coordinator = new Coordinator(_distributedCache);
            string[] deviceCodes = coordinator.ForControlWebSite_GetAllProductCodeWhichDeclaredIP();

            // indirilmiş olan dosyaların bulunduğu yerdeki klasörün yolu elde ediliyor:
            string[] deviceDirectories = Directory.GetDirectories(_env.WebRootPath);

            List<string> list = new List<string>();
            for (int i = 0; i < deviceCodes.Length; i++)
            {
                bool flag = false;
                for (int j = 0; j < deviceDirectories.Length; j++)
                {
                    string directoryName = Path.GetFileName(deviceDirectories[j]);
                    if (deviceCodes[i]==directoryName)
                    {
                        flag = true;
                        break;
                    }
                }
                if (flag==false)
                {
                    list.Add(deviceCodes[i]);
                }
            }

            ViewData["list"] = list.ToArray();

            return View();
        }

        #region Transitions

        public IActionResult Transition_DownloadPackage(string id)
        {
            try
            {
                string productCode = id;

                string deviceDirectory = Path.Combine(_env.WebRootPath, id);

                if (!Directory.Exists(deviceDirectory))
                    Directory.CreateDirectory(deviceDirectory);
                else
                    // ::ERR::
                    return RedirectToAction("Launch");

                string responseString = "";
                using (var client = new HttpClient())
                {
                    var url = "https://otomationstore.azurewebsites.net/api/Service/"+id;
                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        // by calling .Result you are performing a synchronous call
                        var responseContent = response.Content;

                        // by calling .Result you are synchronously reading the result
                        responseString = responseContent.ReadAsStringAsync().Result;
                    }
                    else
                    {
                        // ::ERR::
                        return RedirectToAction("Launch");
                    }
                }

                // save json response as device json file:
                ProductModel pm = (ProductModel)JsonConvert.DeserializeObject(responseString, typeof(ProductModel));
                StreamWriter write = new StreamWriter(Path.Combine(deviceDirectory, pm.product_code + ".json"));
                write.Write(responseString);
                write.Close();

                // download files:
                using (var webClient = new WebClient())
                {
                    // downlaod device image file:
                    string imageFilePath = Path.Combine(deviceDirectory, pm.image_path);
                    string imageFileUrl = "https://otomationstore.azurewebsites.net/Storage/" + pm.producer_name + "/" + pm.product_code + "/" + pm.image_path;
                    webClient.DownloadFile(imageFileUrl, imageFilePath);

                    // downlaod device zip file:
                    string zipFilePath = Path.Combine(deviceDirectory, pm.package_file_path);
                    string zipFileUrl = "https://otomationstore.azurewebsites.net/Storage/" + pm.producer_name + "/" + pm.product_code + "/" + pm.package_file_path;
                    webClient.DownloadFile(zipFileUrl, zipFilePath);
                }


                // extracting zip file:
                string zipPath = Path.Combine(deviceDirectory, pm.package_file_path);
                string extractPath = deviceDirectory;
                ZipFile.ExtractToDirectory(zipPath, extractPath);
                // delete zip file :not need to zip file
                System.IO.File.Delete(zipPath);
                
                // ::OK::
                return RedirectToAction("Launch");
            }
            catch (Exception ex)
            {
                // ::ERR::
                return RedirectToAction("Launch");
            }
        }

        public IActionResult Transition_DeleteNotification(string id)
        {
            try
            {
                Coordinator coordinator = new Coordinator(_distributedCache);
                int notificationId = Convert.ToInt32(id);

                int result=coordinator.ForControlWebSite_DeleteNotification(notificationId);
                if (result==1)
                {
                    
                }
            }
            catch
            {
                            
            }

            return RedirectToAction("Alarms");
        }

        #endregion




        #region Codes

        private List<DeviceModel> GetDeviceApps()
        {
            // tüm uygulamaları deviceModel tipinde temsil edeceğiz. bu liste indirilmiş olan tüm uygulamaları belirtecek.
            List<DeviceModel> deviceApps = new List<DeviceModel>();

            // indirilmiş olan dosyaların bulunduğu yerdeki klasörün yolu elde ediliyor:
            string[] deviceDirectories = Directory.GetDirectories(_env.WebRootPath);

            // indirilen her bir uygulama için bir klasör var o klasör kullanılarak bir DeviceModel nesnesi üretilip listeye ekleniyor
            foreach (string directory in deviceDirectories)
            {
                string deviceCode = Path.GetFileName(directory);
                // cihaza ait tüm bilgiler cihaz.json dosyasında bulunduğu için ilk öncce dosyayı okuyup bilgileri elde ediyoruz.
                StreamReader read = new StreamReader(Path.Combine(_env.WebRootPath, deviceCode, deviceCode + ".json"));
                string deviceJson = read.ReadToEnd();
                read.Close();
                // elde edilen bilgileri kullanıp ProductModel tipine otomatik olarak çeviriyoruz.
                ProductModel pm = (ProductModel)JsonConvert.DeserializeObject(deviceJson,typeof(ProductModel));

                // bu bilgilerden yararlanarak asıl tipimiz olan DeviceModel nesnesini oluşturuyoruz
                DeviceModel deviceModel = new DeviceModel()
                {
                    title = pm.title,
                    imageName = pm.image_path,
                    producerName = pm.producer_name,
                    productCode = pm.product_code
                };

                // oluşturulan model listeye ekleniyor.
                deviceApps.Add(deviceModel);
            }

            // liste kendisini çağırana döndürülüyor.
            return deviceApps;
        }


        #endregion
    }
}