﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using ControlWebSite.Code;
using ControlWebSite.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace ControlWebSite.Controllers
{
    [Route("api/[controller]")]
    public class NotificationController : Controller
    {
        private readonly IDistributedCache _distributedCache;

        public NotificationController(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }
        

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            Coordinator coordinator = new Coordinator(_distributedCache);
            NotificationModel nm= coordinator.ForNotificationHub_GetAllNotifications();

            return JsonConvert.SerializeObject(nm);
        }


        // POST api/<controller>
        [HttpPost]
        public string Post([FromBody]Notification value)
        {
            try
            {
                // notfity coordinator
                Coordinator coordinator = new Coordinator(_distributedCache);
                coordinator.ForWebApi_AddNotification(value);

                return "success";

                // kaydedilen değer Redisten okunup geri döndürülüyor:

                
                //return _distributedCache.GetString(value.productCode);


                // notify all users whos connected with SignalR
                // notify

                // return "success";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }

        //// PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}




        //public string SendMessageToDevice(MessageToDeviceModel message)
        //{
        //    try
        //    {
        //        string deviceCode = message.productCode;
        //        string messageJsonString = JsonConvert.SerializeObject(message);

        //        string deviceIpOnRedis = "IP_" + deviceCode;
        //        string deviceIP = _distributedCache.GetString(deviceIpOnRedis);


        //        // HTTP Post Request to Device Service
        //        var request = (HttpWebRequest)WebRequest.Create("http://"+deviceIP+":5000/api/DeviceService");

        //        var postData = messageJsonString;
        //        var data = Encoding.ASCII.GetBytes(postData);

        //        request.Method = "POST";
        //        request.ContentType = "application/json";
        //        request.ContentLength = data.Length;

        //        using (var stream = request.GetRequestStream())
        //        {
        //            stream.Write(data, 0, data.Length);
        //        }

        //        var response = (HttpWebResponse)request.GetResponse();

        //        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        //        if (responseString == "success")
        //            return "success";
        //        else
        //            return "device_failed";
        //    }
        //    catch (Exception ex)
        //    {
        //        return "server_failed";
        //    }
        //}




    }
}
