﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using ControlWebSite.Code;
using ControlWebSite.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace ControlWebSite.Controllers
{
    [Route("api/[controller]")]
    public class MessageController : Controller
    {
        private readonly IDistributedCache _distributedCache;

        public MessageController(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            DeviceInformationModel dvm = new DeviceInformationModel
            {
                productCode="vm-4000",
            };
            dvm.information = new List<InfoKeyValue>() { new InfoKeyValue() {key="isOpen",value="true" } , new InfoKeyValue() { key = "temprature", value = "35 Celcius" } };
            
            return JsonConvert.SerializeObject(dvm);
        }


        // POST api/<controller>
        [HttpPost]
        public string Post([FromBody]MessageToDeviceModel message)
        {
            try
            {
                DeviceInformationModel value = new DeviceInformationModel()
                {
                    productCode = message.productCode,
                    information = new List<InfoKeyValue>()
                };
                value.information.Add(new InfoKeyValue() { key = message.property, value = message.value });

                // notfity coordinator
                Coordinator coordinator = new Coordinator(_distributedCache);
                coordinator.ForWebApi_AddMessage(value);

                return "success";
            }
            catch (Exception ex)
            {
                return "failed";
            }
        }

    }
}
