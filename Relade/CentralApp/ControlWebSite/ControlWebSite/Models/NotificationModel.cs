﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWebSite.Models
{
    public class NotificationModel
    {
        public List<Notification> notifications { get; set; }
    }

    public class Notification
    {
        public int Id { get; set; }
        public string _type { get; set; }
        public string deviceCode { get; set; }
        public string content { get; set; }
        public string datetime { get; set; }
    } 
}
