﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWebSite.Models
{
    public class OperationResponseModel
    {
        public string operationCode { get; set; }
        public string response { get; set; }

        public OperationResponseModel(string _operationCode,string _response)
        {
            operationCode = _operationCode;
            response = _response;
        }
    }


}
