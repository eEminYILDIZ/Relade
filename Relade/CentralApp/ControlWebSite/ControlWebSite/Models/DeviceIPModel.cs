﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWebSite.Models
{
    public class DeviceIPModel
    {
        public DeviceIPModel()
        {
            list = new List<DeviceIp>();
        }
        public List<DeviceIp> list { get; set; }
    }

    public class DeviceIp
    {
        public string deviceCode { get; set; }
        public string IP { get; set; }
    }
}
