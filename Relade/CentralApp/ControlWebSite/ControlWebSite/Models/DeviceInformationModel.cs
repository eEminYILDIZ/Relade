﻿using System.Collections.Generic;

namespace ControlWebSite.Models
{
    public class DeviceInformationModel
    {
        public string productCode { get; set; }
        public string operationCode { get; set; }
        public List<InfoKeyValue> information { get; set; }
    }

    public class InfoKeyValue
    {
        public string key { get; set; }
        public string value { get; set; }
    }

}