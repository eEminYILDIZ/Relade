﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWebSite.Models
{
    public class DeviceModel
    {
        public string title { get; set; }
        public string productCode { get; set; }
        public string producerName { get; set; }
        public string imageName { get; set; }
    }
}
