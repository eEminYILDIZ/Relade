﻿using ControlWebSite.Code;
using ControlWebSite.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWebSite.Hubs
{
    public class ChatHub : Hub
    {
        public static IHubClients _clients;

        private readonly IDistributedCache _distributedCache;

        public ChatHub(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
            //_clients = Clients;
        }

        public override Task OnConnectedAsync()
        {
            _clients = Clients;

            return base.OnConnectedAsync();
        }

        public Task join()
        {
            return Clients.All.InvokeAsync("joined", "User: " + Context.ConnectionId);
        }

        public Task SendMessageToDevice(DeviceInformationModel message)
        {
            try
            {
                string deviceCode = message.productCode;

                Coordinator coordinator = new Coordinator(_distributedCache);

                // get device ip from redis
                string deviceIP = coordinator.ForSignalR_ChatHub_GetDeviceIP(deviceCode);

                MessageToDeviceModel mtdm = new MessageToDeviceModel()
                {
                    productCode = message.productCode,
                    property = message.information.ElementAt(0).key,
                    value = message.information.ElementAt(0).value
                };

                // send message to device
                int result = coordinator.ForSignalrHub_ChatHub_SendMessageToDevice(mtdm, deviceIP);

                OperationResponseModel responseModel;
                if (result == 1)
                    responseModel = new OperationResponseModel(message.operationCode, "succeded");
                else
                    responseModel = new OperationResponseModel(message.operationCode, "failed");

                // kaydedilen değer Redisten okunup geri döndürülüyor:
                return Clients.Client(Context.ConnectionId).InvokeAsync("OperationResponse", responseModel);
            }
            catch (Exception ex)
            {
                OperationResponseModel responseModel = new OperationResponseModel(message.operationCode, "failed");
                return Clients.All.InvokeAsync("OperationResponse", responseModel);
            }

        }

        public static Task SendUsers(DeviceInformationModel message)
        {
            return _clients.All.InvokeAsync("MessageFromDevice", message);
        }

        public Task GetAllDeviceInformations(DeviceInformationModel message)
        {
            Coordinator coordinator = new Coordinator(_distributedCache);
            DeviceInformationModel dvm = coordinator.ForSignalRHub_GetAllDeviceInformations(message.productCode);
            return Clients.Client(Context.ConnectionId).InvokeAsync("MessageFromDevice", dvm);
        }
    }
}
