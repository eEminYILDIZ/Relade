﻿using ControlWebSite.Code;
using ControlWebSite.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWebSite.Hubs
{
    public class NotificationHub : Hub
    {
        public static IHubClients _clients;

        private readonly IDistributedCache _distributedCache;

        public NotificationHub(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
            //_clients = Clients;
        }

        public override Task OnConnectedAsync()
        {
            _clients = Clients;

            return base.OnConnectedAsync();
        }

        public Task GetAllNotifications()
        {
            try
            {
                Coordinator coordinator = new Coordinator(_distributedCache);
                NotificationModel nm = coordinator.ForNotificationHub_GetAllNotifications();

                // kaydedilen değer Redisten okunup geri döndürülüyor:
                return Clients.Client(Context.ConnectionId).InvokeAsync("NotifiedAll", nm);
            }
            catch (Exception ex)
            {
                NotificationModel nm = new NotificationModel()
                { notifications = new List<Notification>() };

                // kaydedilen değer Redisten okunup geri döndürülüyor:
                return Clients.Client(Context.ConnectionId).InvokeAsync("NotifiedAll", nm);
            }

        }

        public static Task SendUsers(NotificationModel nm)

        {
            return _clients.All.InvokeAsync("NotifiedOnce", nm);
        }
    }
}
