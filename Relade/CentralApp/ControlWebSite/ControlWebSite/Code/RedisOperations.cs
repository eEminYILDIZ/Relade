﻿using ControlWebSite.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWebSite.Code
{
    public class RedisOperations
    {

        private readonly IDistributedCache _distributedCache;

        public RedisOperations(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }


        public int Write_DeviceInfo(DeviceInformationModel value)
        {
            try
            {
                //
                string oldData = _distributedCache.GetString(value.productCode);
                // eğer gönderilen cihaz bilgileri sistemde ilk defa oluşturulacaksa:
                if (oldData == null)
                {
                    // gelen mesaj modeli json tipinde string'e çevriliyor:
                    string data = JsonConvert.SerializeObject(value);

                    // elde edilen string REDİS üzerinde kaydediliyor:
                    _distributedCache.SetString(value.productCode, data);
                }
                // eğer gönderilen değer bir veri güncellemesiyse
                else
                {
                    // gelen verinin sahibi olan cihaz adından Redis'ten o cihaza ait olan veriler json string'i olarak alınıyor:
                    string jsonData = _distributedCache.GetString(value.productCode);
                    // alınan json string'inden DeviceInformationModel nesnesi oluşturuluyor:
                    DeviceInformationModel dvm = (DeviceInformationModel)JsonConvert.DeserializeObject(jsonData, typeof(DeviceInformationModel));

                    if (dvm.information == null)
                    {
                        dvm.information = new List<InfoKeyValue>();
                    }

                    bool flag = false;
                    // güncellenmesi istenen her değer için redis üzerindeki değerin aynısı bulunup değiştiriliyor:
                    foreach (InfoKeyValue infoKeyValue in value.information)
                    {
                        InfoKeyValue ifk = dvm.information.Where(w => w.key == infoKeyValue.key).FirstOrDefault();
                        if (ifk != null)
                        {
                            flag = true;
                            ifk.value = infoKeyValue.value;
                            break;
                        }
                    }
                    // if key not found at the old data
                    if (!flag)
                    {
                        dvm.information.Add(value.information.ElementAt(0));
                    }

                    // güncelleştirilmiş asıl veri json olarak string'e çevriliyor:
                    string data = JsonConvert.SerializeObject(dvm);

                    // güncelleştirilmiş json string'i Redis'e yazılıyor:
                    _distributedCache.SetString(value.productCode, data);

                }

                return 1;
            }
            catch (Exception ex)
            {
                return -1;

            }
        }




        public int Write_Notification(Notification value)
        {
            try
            {
                //
                string oldData = _distributedCache.GetString("notifications");
                // eğer gönderilen cihaz bilgileri sistemde ilk defa oluşturulacaksa:
                if (oldData == null)
                {
                    NotificationModel nm = new NotificationModel()
                    {
                        notifications = new List<Notification>(),
                    };
                    nm.notifications.Add(value);

                    // gelen mesaj modeli json tipinde string'e çevriliyor:
                    string data = JsonConvert.SerializeObject(nm);

                    // elde edilen string REDİS üzerinde kaydediliyor:
                    _distributedCache.SetString("notifications", data);
                }
                // eğer gönderilen değer bir veri güncellemesiyse
                else
                {
                    // gelen verinin sahibi olan cihaz adından Redis'ten o cihaza ait olan veriler json string'i olarak alınıyor:
                    string jsonData = _distributedCache.GetString("notifications");
                    // alınan json string'inden DeviceInformationModel nesnesi oluşturuluyor:
                    NotificationModel nModel = (NotificationModel)JsonConvert.DeserializeObject(jsonData, typeof(NotificationModel));

                    bool flag = false;

                    Notification foundnf = nModel.notifications.Where(w => w.Id == value.Id).FirstOrDefault();

                    if (foundnf != null)
                    {
                        flag = true;
                    }

                    //if this is new notification
                    if (!flag)
                    {
                        nModel.notifications.Add(value);

                        // güncelleştirilmiş asıl veri json olarak string'e çevriliyor:
                        string data = JsonConvert.SerializeObject(nModel);

                        // güncelleştirilmiş json string'i Redis'e yazılıyor:
                        _distributedCache.SetString("notifications", data);
                    }

                }

                return 1;
            }
            catch (Exception ex)
            {
                return -1;

            }
        }

        public int DeleteNotification(int id)
        {
            try
            {
                //
                string oldData = _distributedCache.GetString("notifications");
                // eğer gönderilen cihaz bilgileri sistemde ilk defa oluşturulacaksa:
                if (oldData == null)
                {
                    
                }
                // eğer gönderilen değer bir veri güncellemesiyse
                else
                {
                    // gelen verinin sahibi olan cihaz adından Redis'ten o cihaza ait olan veriler json string'i olarak alınıyor:
                    string jsonData = _distributedCache.GetString("notifications");
                    // alınan json string'inden DeviceInformationModel nesnesi oluşturuluyor:
                    NotificationModel nModel = (NotificationModel)JsonConvert.DeserializeObject(jsonData, typeof(NotificationModel));

                    bool flag = false;

                    Notification foundnf = nModel.notifications.Where(w => w.Id == id).FirstOrDefault();

                    if (foundnf != null)
                    {
                     
                        nModel.notifications.Remove(foundnf);

                        // güncelleştirilmiş asıl veri json olarak string'e çevriliyor:
                        string data = JsonConvert.SerializeObject(nModel);

                        // güncelleştirilmiş json string'i Redis'e yazılıyor:
                        _distributedCache.SetString("notifications", data);
                    }

                }

                return 1;
            }
            catch (Exception ex)
            {
                return -1;

            }
        }

        public NotificationModel GetAllNotifications()
        {
            // gelen verinin sahibi olan cihaz adından Redis'ten o cihaza ait olan veriler json string'i olarak alınıyor:
            string jsonData = _distributedCache.GetString("notifications");
            NotificationModel nModel;

            if (jsonData != null)
            {
                // alınan json string'inden DeviceInformationModel nesnesi oluşturuluyor:
                nModel = (NotificationModel)JsonConvert.DeserializeObject(jsonData, typeof(NotificationModel));

            }
            else
            {
                nModel = new NotificationModel()
                {
                    notifications = new List<Notification>(),
                };
            }

            return nModel;
        }



        public string[] GetAllProductCodeWhichDeclaredIP()
        {
            string deviceIpKey = "deviceIps";
            // Redis'ten bitin cihazlara ait Ip adresleri ve ürün kodları alınıyor.
            string jsonData = _distributedCache.GetString(deviceIpKey);
            // alınan json string'inden DeviceInformationModel nesnesi oluşturuluyor:
            DeviceIPModel dip = (DeviceIPModel)JsonConvert.DeserializeObject(jsonData, typeof(DeviceIPModel));
            List<string> list = new List<string>();
            foreach (DeviceIp eleman in dip.list)
            {
                list.Add(eleman.deviceCode);
            }
            return list.ToArray();
        }



        public DeviceInformationModel GetAllDeviceInformations(string productCode)
        {
            // gelen verinin sahibi olan cihaz adından Redis'ten o cihaza ait olan veriler json string'i olarak alınıyor:
            string jsonData = _distributedCache.GetString(productCode);
            // alınan json string'inden DeviceInformationModel nesnesi oluşturuluyor:
            DeviceInformationModel dvm = (DeviceInformationModel)JsonConvert.DeserializeObject(jsonData, typeof(DeviceInformationModel));

            if (dvm.information == null)
            {
                dvm.information = new List<InfoKeyValue>();
            }

            return dvm;
        }






        public int Write_DeviceIP(DeviceIp value)
        {
            try
            {
                string deviceIpKey = "deviceIps";
                //
                // string oldData = _distributedCache.GetString(deviceIpKey);
                string oldData = null;
                // eğer gönderilen cihaz bilgileri sistemde ilk defa oluşturulacaksa:
                if (oldData == null)
                {
                    DeviceIPModel devIpModel = new DeviceIPModel();
                    devIpModel.list.Add(value);

                    // gelen mesaj modeli json tipinde string'e çevriliyor:
                    string data = JsonConvert.SerializeObject(devIpModel);

                    // elde edilen string REDİS üzerinde kaydediliyor:
                    _distributedCache.SetString(deviceIpKey, data);
                }
                // eğer gönderilen değer bir veri güncellemesiyse
                else
                {
                    // gelen verinin sahibi olan cihaz adından Redis'ten o cihaza ait olan veriler json string'i olarak alınıyor:
                    string jsonData = _distributedCache.GetString(deviceIpKey);
                    // alınan json string'inden DeviceInformationModel nesnesi oluşturuluyor:
                    DeviceIPModel dip = (DeviceIPModel)JsonConvert.DeserializeObject(jsonData, typeof(DeviceIPModel));

                    bool flag = false;
                    // güncellenmesi istenen her değer için redis üzerindeki değerin aynısı bulunup değiştiriliyor:

                    DeviceIp devIP = dip.list.Where(w => w.deviceCode == value.deviceCode).FirstOrDefault();
                    if (devIP != null)
                    {
                        flag = true;
                        devIP.IP = value.IP;
                    }
                    // if key not found at the old data
                    if (!flag)
                    {
                        dip.list.Add(value);
                    }

                    // güncelleştirilmiş asıl veri json olarak string'e çevriliyor:
                    string data = JsonConvert.SerializeObject(dip);

                    // güncelleştirilmiş json string'i Redis'e yazılıyor:
                    _distributedCache.SetString(deviceIpKey, data);
                }

                return 1;
            }
            catch (Exception ex)
            {
                return -1;

            }
        }

        public string Read_DeviceIP(string deviceCode)
        {
            try
            {
                string deviceIpKey = "deviceIps";
                //
                string oldData = _distributedCache.GetString(deviceIpKey);
                // eğer gönderilen cihaz bilgileri sistemde ilk defa oluşturulacaksa:
                if (oldData != null)
                {
                    DeviceIPModel dvm = (DeviceIPModel)JsonConvert.DeserializeObject(oldData, typeof(DeviceIPModel));

                    DeviceIp devIP = dvm.list.Where(w => w.deviceCode == deviceCode).FirstOrDefault();
                    if (devIP != null)
                        return devIP.IP;
                    else
                        return "127.0.0.1";
                }
                // eğer gönderilen değer bir veri güncellemesiyse
                else
                {
                    return "127.0.0.1";
                }
            }
            catch (Exception ex)
            {
                return "127.0.0.1";
            }
        }
    }
}
