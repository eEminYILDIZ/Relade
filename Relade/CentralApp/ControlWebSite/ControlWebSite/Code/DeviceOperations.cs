﻿using ControlWebSite.Data;
using ControlWebSite.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ControlWebSite.Code
{
    public class DeviceOperations
    {
        public static int SendCommand(MessageToDeviceModel message, string deviceIP)
        {
            try
            {
                deviceIP = "localhost";
                string messageJsonString = JsonConvert.SerializeObject(message);
                string port = General_Values.deviceWebApiListenPort;
                // HTTP Post Request to Device Service
                var request = (HttpWebRequest)WebRequest.Create("http://" + deviceIP + ":"+port+"/api/Service");

                var postData = messageJsonString;
                var data = Encoding.ASCII.GetBytes(postData);

                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = data.Length;

                using (var stream = request.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                var response = (HttpWebResponse)request.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                if (responseString == "success")
                    return 1;
                else
                    return 0;
            }
            catch (Exception ex)
            {
                return -1;
            }



        }
    }
}
