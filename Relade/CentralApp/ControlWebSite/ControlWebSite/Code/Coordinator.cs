﻿using ControlWebSite.Hubs;
using ControlWebSite.Models;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWebSite.Code
{
    public class Coordinator
    {

        private readonly IDistributedCache _distributedCache;

        public Coordinator(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }



        public int ForWebApi_AddMessage(DeviceInformationModel value)
        {
            try
            {
                RedisOperations redisOperation = new RedisOperations(_distributedCache);
                int result=redisOperation.Write_DeviceInfo(value);

                if (result != 1)
                    return -1;


                // notfity all user over signalr
                //ChatHub chatHub = new ChatHub(_distributedCache);
                ChatHub.SendUsers(value);



                // succeded
                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }


        public int ForSignalrHub_ChatHub_SendMessageToDevice(MessageToDeviceModel message,string deviceIP)
        {
            try
            {                
                //notfity device 
                int result=DeviceOperations.SendCommand(message,deviceIP);
                                
                return result;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public DeviceInformationModel ForSignalRHub_GetAllDeviceInformations(string productCode)
        {
            RedisOperations redisOperation = new RedisOperations(_distributedCache);
            return redisOperation.GetAllDeviceInformations(productCode);
        }

        public int ForWebApi_AddNotification(Notification value)
        {
            try
            {
                RedisOperations redisOperation = new RedisOperations(_distributedCache);
                int result = redisOperation.Write_Notification(value);

                if (result != 1)
                    return -1;



                // notfity all user over signalr
                //ChatHub chatHub = new ChatHub(_distributedCache);
                NotificationModel nm = new NotificationModel()
                {
                    notifications = new List<Notification>(),
                };
                nm.notifications.Add(value);

                NotificationHub.SendUsers(nm);
                
                // succeded
                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }


        public NotificationModel ForNotificationHub_GetAllNotifications()
        {
            RedisOperations redisOperation = new RedisOperations(_distributedCache);
            return redisOperation.GetAllNotifications();
        }

        public string[] ForControlWebSite_GetAllProductCodeWhichDeclaredIP()
        {
            RedisOperations redisOperation = new RedisOperations(_distributedCache);
            return redisOperation.GetAllProductCodeWhichDeclaredIP();
        }

        public int ForControlWebSite_DeleteNotification(int id)
        {
            RedisOperations redisOperation = new RedisOperations(_distributedCache);
            return redisOperation.DeleteNotification(id);
        }


        public int ForWebApi_SetDeviceIP(DeviceIp value)
        {
            try
            {
                RedisOperations redisOperation = new RedisOperations(_distributedCache);
                int result = redisOperation.Write_DeviceIP(value);

                if (result != 1)
                    return -1;
                
                // succeded
                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public string ForSignalR_ChatHub_GetDeviceIP(string deviceCode)
        {
            RedisOperations rop = new RedisOperations(_distributedCache);
            string _return = rop.Read_DeviceIP(deviceCode);
            return _return;
        }
    }
}
