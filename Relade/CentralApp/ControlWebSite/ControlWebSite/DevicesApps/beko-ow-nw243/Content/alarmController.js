function PlaySound() {
    var sound = document.getElementById("sound1");
    sound.Play();
}
// ------------------------------------------------------------------


var app2 = angular.module('alarmApp', []);

app2.controller('alarmController', function ($scope) {

    $scope.notifications = [{}];
    
    // SignalR nesnesi oluşturuluyor:
    connection = new signalR.HubConnection('/notification');

    // Sunucunun Çağırdığı Metot: (list contains all element)    .
    connection.on('NotifiedAll', data => {

        $scope.notifications = data.notifications;
        $scope.$apply(function () {
        });

        
    });

    // Sunucunun Çağırdığı Metot: (list contains just one element)    .
    connection.on('NotifiedOnce', data => {

        $scope.notifications.push(data);
        $scope.$apply(function () {
        });

        for (var i = 0; i < data.notifications.length; i++) {
            var item = data.notifications[i];

            PlaySound();

            switch (item._type) {
                case 'alarm':
                    window.location.href = "/Control/Alarms";
                    break;

                default:
                    break;
            }
        }


    });


    
  
    connection.start().then(() => connection.invoke('GetAllNotifications'));

    $scope.channelDown = function () {
        
    }


});

angular.bootstrap(document.getElementById("alarmApp"), ['alarmApp']);