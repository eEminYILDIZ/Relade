
            app.controller('deviceController', function ($scope) {

                $scope.name = "Samsung Smart TV";
            });

            app.controller('clicks', function ($scope) {

                // SignalR nesnesi oluşturuluyor:
                connection = new signalR.HubConnection('/chat');

                // Sunucunun Çağırdığı Metot:
                connection.on('MessageFromDevice', data => {
                    //var obj = JSON.parse(data);
                    //alert(JSON.stringify);
                    MapPropertyScope(data);
                });

                connection.on('OperationResponse', data => {
                    try {
                        //alert(data.operationCode + " >> " + data.response);
                    } catch (e) {
                        alert(e);
                    }
                });



                // Sunucuya İsteğin Yapıldığı Yer:
                var SetUpdateInformation = function (data) {
                    connection.invoke('SendMessageToDevice', data);
                }

                // bağlandığı an çalışacak olan metot:
                var pCode = 'smsng-tv-e7400';

                var data = {
                    productCode: pCode, operationCode: GetOperationCode(), information: [{ key: 'isOpen', value: 'ACIK' }]
                };


                connection.start().then(() => connection.invoke('GetAllDeviceInformations', data));



                function MapPropertyScope(data) {
                    for (var i = 0; i < data.information.length; i++) {
                        var item = data.information[i];

                        switch (item.key) {
                            case 'openClose':
                                $scope.isOpen = item.value;
                                break;

                            case 'channel':
                                $scope.channel = item.value;
                                break;

                            case 'volume':
                                $scope.volume = item.value;
                                break;
                            default:
                        }
                    }

                    $scope.$apply(function () {
                    });
                }

                var opCode = 0;
                function GetOperationCode() {
                    opCode++;
                    return "" + opCode;
                }

                $scope.isOpen = "?";
                $scope.channel = "?";
                $scope.volume = "?";

                $scope.openClose = function () {
                    var newIsOpen;
                    if ($scope.isOpen === 'ACIK') {
                        newIsOpen = 'KAPALI';
                    } else {
                        newIsOpen = 'ACIK';
                    }



                    var data = {
                        productCode: pCode, operationCode: GetOperationCode(), information: [{ key: 'openClose', value: newIsOpen }]
                    };

                    SetUpdateInformation(data);

                }

                $scope.volumeUp = function () {

                    var newVolume;
                    if ($scope.volume === '?') {
                        newVolume = 0;
                    } else {
                        newVolume = parseInt($scope.volume) + 1;
                    }

                    var data = {
                        productCode: pCode, operationCode: GetOperationCode(), information: [{ key: 'volume', value: newVolume }]
                    };
                    SetUpdateInformation(data);
                }

                $scope.volumeDown = function () {

                    var newVolume;
                    if ($scope.volume === '?') {
                        newVolume = 0;
                    } else {
                        newVolume = parseInt($scope.volume) - 1;
                    }

                    var data = {
                        productCode: pCode, operationCode: GetOperationCode(), information: [{ key: 'volume', value: newVolume }]
                    };
                    SetUpdateInformation(data);
                }

                $scope.channelUp = function () {
                    var newChannel;
                    if ($scope.channel === '?') {
                        newChannel = 0;
                    } else {
                        newChannel = parseInt($scope.channel) + 1;
                    }

                    var data = {
                        productCode: pCode, operationCode: GetOperationCode(), information: [{ key: 'channel', value: newChannel }]
                    };
                    SetUpdateInformation(data);
                }

                $scope.channelDown = function () {
                    var newChannel;
                    if ($scope.channel === '?') {
                        newChannel = 0;
                    } else {
                        newChannel = parseInt($scope.channel) - 1;
                    }

                    var data = {
                        productCode: pCode, operationCode: GetOperationCode(), information: [{ key: 'channel', value: newChannel }]
                    };
                    SetUpdateInformation(data);
                }


            });